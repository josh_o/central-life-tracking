# README #

### Central Life Tracking System ###

* A life tracking system developed for use with the Raspberry Pi
* Integrated with a web page to enable mobile updating
* Add items to a shopping list, to-do list or calendar via the web interface
* View all items on a GUI on the Pi (Designed for use with a touch screen connected to the Pi)
* Get alerted when new items have been received via an LED
* Version 0.0.1

### Who do I talk to? ###

* Email josh@joshoxe.com